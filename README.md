Nano-HCM
===========================
Code and examples to compute the Hausdorff Chirality Measure (HCM)
for metal nanoclusters (see [1] and references therein).  

Note: the code currently relies on a not-so-efficient brute
force search for the rotations. Additionally translations of
the cluster are not yet included.

[1] I.L. Garzón et al., http://dx.doi.org/10.1140/epjd/e2003-00187-4
