from ase.cluster import Octahedron
from hcm import calculate_hcm

# Truncated octahedron (38 atoms)
atoms = Octahedron('Au', 4, cutoff=1)
atoms.center(vacuum=10)

hcm = calculate_hcm(atoms, angle=2, eps=1e-10)
print 'HCM = ' + str(hcm)
assert hcm == 0
