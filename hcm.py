from __future__ import print_function
import numpy as np
from scipy.spatial.distance import directed_hausdorff

def calculate_hd(a, b):
    ''' Return Hausdorff distance between two Atoms objects '''
    u = a.get_positions()
    v = b.get_positions()
    hd, i1, i2 = directed_hausdorff(u, v)
    return hd

def calculate_hcm(atoms, angle=2, eps=1e-10):
    ''' 
    Computes the Hausdorff Chirality Measure

    atoms: an Atoms object
    angle: angle increment (in degrees) to be 
           used in rotating the structure
    eps: if the Hausdorff distance is below this
         value, it is considered equal to zero
    '''
    # Cartesian coordinates
    q = atoms.get_positions()
    # Maximum distance
    dq = np.max(atoms.get_all_distances())
    # Transform w.r.t. cluster COM
    com = atoms.get_center_of_mass()
    qq = np.copy(q)
    qq += 2*(com - q)
    atoms2 = atoms.copy()
    atoms2.set_positions(qq)
    # Compute Hausdorff distance
    hd = calculate_hd(atoms, atoms2)

    if hd < eps:
        # Q is achiral
        return 0
    else:
        # Consider all rotations 
        hds = []
        phis = np.arange(0, 180, angle)
        thetas = np.arange(0, 180, angle)
        psis = np.arange(0, 91, angle)
        progress = 0

        for phi in phis:
            for theta in thetas:
                for psi in psis:
                    atoms2.set_positions(qq)
                    atoms2.euler_rotate(phi=phi, theta=theta, psi=psi, 
                                        center='COM')
                    hd = calculate_hd(atoms, atoms2)
                    if hd < eps:
                        # Q is achiral
                        return 0
                    else:
                        hds.append(hd) 

            if 100*phi/np.max(phis) >= progress + 10:
                progress += 10
                hcm = np.min(hds)/dq
                print('Progress = %02d %%, current HCM = %.4f'  % \
                      (progress, hcm))

        return np.min(hds)/dq
